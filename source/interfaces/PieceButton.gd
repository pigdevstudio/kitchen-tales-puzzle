extends Button
"""
A Button that instantiate a scene and signs its instantiation
"""

signal scene_instanciated(instance)

export (PackedScene) var scene

func _on_button_down():
	var i = scene.instance()
	emit_signal("scene_instanciated", i)
