extends Control

var piece

func _ready():
	set_process(false)
	$DragDrop.grid = $TileMap

func _process(delta):
	$DragDrop.drag(piece)
	if Input.is_action_just_pressed("left"):
		$Rotation.rotate(piece, deg2rad(-90))
	elif Input.is_action_just_pressed("right"):
		$Rotation.rotate(piece, deg2rad(90))
	
func _on_DragDrop_dropped():
	set_process(false)

func _on_piece_added(instance):
	set_process(true)
	piece = instance
