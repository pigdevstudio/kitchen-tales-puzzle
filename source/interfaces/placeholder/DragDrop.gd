extends Node
"""
Sets a Node2D's global position to the mouse global position
"""

signal dropped

func drag(node2d):
	node2d.global_position = node2d.get_global_mouse_position()

func _input(event):
	if event.is_action_released("left_click"):
		emit_signal("dropped")
