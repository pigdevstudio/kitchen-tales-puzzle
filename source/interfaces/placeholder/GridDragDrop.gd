extends "res://interfaces/placeholder/DragDrop.gd"

var grid

func drag(node2d):
	var cell = grid.world_to_map(grid.get_global_mouse_position())
	node2d.global_position = grid.map_to_world(cell)
