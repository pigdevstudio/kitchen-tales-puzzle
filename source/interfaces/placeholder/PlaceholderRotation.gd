extends Node
"""
Rotates a Node2D by 90 offset based on inputs
"""

func rotate(node2D, angle):
	node2D.rotate(angle)
