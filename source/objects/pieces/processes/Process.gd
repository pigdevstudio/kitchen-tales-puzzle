extends Node
"""
A class that changes the state of an ingredient based on a process.
"""
signal process_finished(ingredient)

enum Processes{Gather, Slice, Fry}

export (Processes) var process = 0

func process_ingredient(ingredient):
	
	$Timer.start()
	yield($Timer, "timeout")
	ingredient.state = process
	emit_signal("process_finished", ingredient)
